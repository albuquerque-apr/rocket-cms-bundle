<?php

namespace Rocket\CmsBundle\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * This is the class that loads and manages your bundle configuration
 *
 * To learn more see {@link http://symfony.com/doc/current/cookbook/bundles/extension.html}
 */
class CmsExtension extends Extension
{
    /**
     * {@inheritDoc}
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__.'/../Resources/config'));
        $loader->load('services.yml');

        $container->setParameter('cms.frontend_template', $config['frontend_template']);
        $container->setParameter('cms.backend_template', $config['backend_template']);
        $container->setParameter('cms.content_form_theme', $config['content_form_theme']);
        $container->setParameter('cms.serialized_data_form_theme', $config['serialized_data_form_theme']);
        $container->setParameter('cms.base_url', $config['base_url']);
        $container->setParameter('cms.files_path', $config['files_path']);
        $container->setParameter('cms.page_content_template', $config['page_content_template']);
        $container->setParameter('cms.aws_s3_use', $config['aws_s3_use']);
        $container->setParameter('cms.aws_s3_secret', $config['aws_s3_secret']);
        $container->setParameter('cms.aws_s3_key', $config['aws_s3_key']);
        $container->setParameter('cms.aws_s3_bucketname', $config['aws_s3_bucketname']);
        $container->setParameter('cms.save_in_server', $config['save_in_server']);
    }
}
