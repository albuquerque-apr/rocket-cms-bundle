<?php

namespace Rocket\CmsBundle\Service\S3;

/**
 * Interface for S3 adapters
 *
 * @author Daniel Campos <daniel.campos@clickbus.com.br>
 */
interface AdapterInterface
{

    /**
     * Send Object to S3 bucket
     *
     * @param string $object
     * @param string $content
     *
     * @return bool
     */
    public function put(
        $object,
        $content
    );

    /**
     * Get specific object from S3 bucket
     *
     * @param string $object
     *
     * @return array Format array('body' => (string))
     */
    public function get(
        $object
    );

    /**
     * Delete specific object from S3 bucket
     *
     * @return bool
     */
    public function delete();

    /**
     * List objects in S3 bucket
     *
     * @param string $object
     *
     * @return array Format array('body' => (string))
     */
    public function list(
        $object
    );
}

