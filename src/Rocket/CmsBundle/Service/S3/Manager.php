<?php

namespace Rocket\CmsBundle\Service\S3;

use Aws\S3\S3Client;

/**
 * S3 Manager
 *
 * @author Daniel Campos <daniel.campos@clickbus.com.br>
 */
class Manager
{
    /**
     * AWS Access Key
     *
     * @var string
     */
    protected $accessKey;

    /**
     * AWS Secret Key
     *
     * @var string
     */
    protected $secretKey;

    /**
     * AWS S3 Bucket Name
     *
     * @var string
     */
    protected $bucketName;

    /**
     * AWS S3 Bucket Client
     *
     * @var string
     */
    protected $bucketClient;

    public function __construct($awsBucketname, $awsKey, $awsSecret)
    {
        if (!isset($awsBucketname) || !isset($awsKey) || !isset($awsSecret)) {
            throw new ConfigurationException('Missing AWS authentication info in configuration file.');
        }
        $this->accessKey = $awsKey;
        $this->secretKey = $awsSecret;
        $this->bucketName = $awsBucketname;
        $this->bucketClient = S3Client::factory(array(
            'key'    => $awsKey,
            'secret' => $awsSecret,
            'region' => 'sa-east-1'
            ));

        return $this->bucketClient;
    }

    public function getBucketClient(){
        return $this->bucketClient;
    }

    public function put($name, $content) 
    {
        return $this->bucketClient->putObject(array(
            'Bucket' => $this->bucketName,
            'Key'    => $name,
            'Body'   => $content
            ));
    }

    public function putFile($name, $path) 
    {
        $res = $this->bucketClient->putObject(array(
            'Bucket' => $this->bucketName,
            'Key'    => $name,
            'SourceFile'   => $path
            ));

        // poll the object until it is accessible
        $this->bucketClient->waitUntilObjectExists(array(
            'Bucket' => $this->bucketName,
            'Key'    => $name
            ));

        return $res;
    }

    public function get($name) 
    {
        return $this->bucketClient->getObject(array(
            'Bucket' => $this->bucketName,
            'Key'    => $name
            ));
    }

    public function delete($name) 
    {
        return $this->bucketClient->deleteObject(array(
            'Bucket' => $this->bucketName,
            'Key'    => $name
            ));
    }

    public function listObjects() 
    {
        return $this->bucketClient->getListObjectsIterator(array('Bucket' => $this->bucketName));
    }

}
