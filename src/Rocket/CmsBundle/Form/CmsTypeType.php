<?php

namespace Rocket\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Symfony\Component\Validator\Constraints as Assert;

class CmsTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('class', 'text', array(
                'constraints' => array(
                    new Assert\NotBlank(array(
                        'message' => 'backend.cmstype.error.class'
                    ))
                )
            ))
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Rocket\CmsBundle\Entity\CmsType',
            'label_prefix' => 'backend.cmstype.'
        ));
    }

    public function getName()
    {
        return 'rocket_cmsbundle_cmstypetype';
    }
}
