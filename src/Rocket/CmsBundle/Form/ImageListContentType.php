<?php

namespace Rocket\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImageListContentType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', 'text', array(
                'constraints' => array(
                    new Constraints\NotBlank(),
                ),
            ))
            ->add('image_files', 'collection', array(
                'type' => 'file',
                'allow_add' => true,
                'allow_delete' => true,
            ))
            ->add('images', 'collection', array(
                'type' => 'hidden',
            ))
            ->add('remove_images', 'collection', array(
                'type' => 'hidden',
            ));
        ;
    }

    /**
     * Disabling CSRF protection
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'csrf_protection' => false,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rocket_cmsbundle_imagelisttype';
    }
}
