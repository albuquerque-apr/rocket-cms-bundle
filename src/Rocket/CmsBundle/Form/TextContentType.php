<?php

namespace Rocket\CmsBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints;

class TextContentType extends AbstractType
{
    /**
     * @param \Symfony\Component\Form\FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('content', 'textarea', array(
                'attr' => array(
                    'class' => 'ckeditor',
                ),
                'constraints' => array(
                    new Constraints\NotBlank(),
                ),
            ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'rocket_cmsbundle_textcontenttype';
    }
}
