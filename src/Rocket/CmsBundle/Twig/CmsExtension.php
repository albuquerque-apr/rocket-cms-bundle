<?php
namespace Rocket\CmsBundle\Twig;

use Symfony\Component\Routing\Router;
use Rocket\CmsBundle\Content\BlankContent;
use Rocket\CmsBundle\Entity\CmsContent;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * CMS Twig extension
 *
 * @author Fernando Carletti <fernando.carletti@rocket-internet.com.br>
 */
class CmsExtension extends \Twig_Extension
{
    /**
     * Initialize class variables
     *
     * @param \Symfony\Component\DependencyInjection\ContainerInterface $container  Service container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * Return twig functions
     *
     * @return array
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('cms_path', array($this, 'cmsPathFunction')),
            new \Twig_SimpleFunction('cms_file_path', array($this, 'cmsFilePathFunction')),
            new \Twig_SimpleFunction('cms_block', array($this, 'cmsBlockFunction')),
        );
    }

    /**
     * Return cms path
     *
     * @param  string $cmsContentName Page name
     * @param  array  $customAttr     Custom html attributes
     *
     * @return string HTML anchor tag
     */
    public function cmsPathFunction($cmsContentName, array $customAttr = array())
    {
        $router = $this->container->get('router');
        $request = $this->container->get('request');
        $cms = $this->container->get('rocket.cms');
        $cmsContent = $cms->findByName($cmsContentName, $request->getLocale());

        if ($cmsContent instanceof BlankContent) {
            $uri = $cmsContentName;
            $title = $cmsContentName;
        } else {
            $serializedData = $cmsContent->getEntity()->getSerializedData();
            $title = isset($serializedData['title']) ? $serializedData['title'] : $cmsContentName;
            $uri = $cmsContent->getEntity()->getUri();
        }

        $path = $router->generate('frontend_cms', array('uri' => $uri));
        $attrs = '';

        foreach ($customAttr as $key => $value) {
            $attrs .= sprintf(' %s="%s" ', $key, $value);
        }

        return sprintf('<a %s href="%s">%s</a>', $attrs, $path, $title);
    }

    /**
     * Render a cms block
     *
     * @param  string $cmsContentName Block name
     *
     * @return string CMS block html
     */
    public function cmsBlockFunction($cmsContentName)
    {
        $cms = $this->container->get('rocket.cms');
        $request = $this->container->get('request');
        $cmsContent = $cms->findByName($cmsContentName, $request->getLocale());

        return $cmsContent->render();
    }

    /**
     * Find and CmsContent by its name
     *
     * @param string $cmsContentName CMS content name
     *
     * @return [type] [description]
     */
    public function findByName($cmsContentName)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $request = $this->container->get('request');
        $cms = $this->container->get('rocket.cms');

        $cmsContentRepository = $em->getRepository('SharedBundle:CmsContent');
        $cmsContent = $cmsContentRepository->findOneWithLocale($request->getLocale(), 'name', $cmsContentName);

        return $cmsContent;
    }

    /**
     * Return cms file path
     *
     * @param string $filename
     *
     * @return string CMS file url
     */
    public function cmsFilePathFunction($filename)
    {
        $baseUrl = $this->container->getParameter('cms.base_url');

        return $baseUrl . $filename;
    }

    /**
     * Extension name
     *
     * @return string
     */
    public function getName()
    {
        return 'rocket_cms_extension';
    }
}
