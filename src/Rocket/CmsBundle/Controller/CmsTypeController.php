<?php

namespace Rocket\CmsBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Rocket\CmsBundle\Entity\CmsType;
use Rocket\CmsBundle\Form\CmsTypeType;

/**
 * CmsType controller.
 *
 * @Route("/cmstype")
 */
class CmsTypeController extends Controller
{
    /**
     * Lists all CmsType entities.
     *
     * @Route("/")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $repository = $this->getDoctrine()->getRepository('CmsBundle:CmsType');
        $entities = $repository->findAll();

        return array(
            'entities' => $entities,
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Creates a new CmsType entity.
     *
     * @Route("/")
     * @Method("POST")
     * @Template()
     */
    public function createAction(Request $request)
    {
        $entity  = new CmsType();
        $form = $this->createForm(new CmsTypeType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('rocket_cms_cmstype_show', array('id' => $entity->getId())));
        }

        return array(
            'entity' => $entity,
            'form'   => $form->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Displays a form to create a new CmsType entity.
     *
     * @Route("/new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $cmsType = new CmsType();
        $form = $this->createForm(new CmsTypeType(), $cmsType);

        return array(
            'entity' => $cmsType,
            'form'   => $form->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Finds and displays a CmsType entity.
     *
     * @Route("/{id}")
     * @Method("GET")
     * @Template()
     * @ParamConverter("cmsType", class="CmsBundle:CmsType")
     */
    public function showAction(CmsType $cmsType)
    {
        if (!$cmsType) {
            throw $this->createNotFoundException('Unable to find CmsType entity.');
        }

        return array(
            'entity' => $cmsType,
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Displays a form to edit an existing CmsType entity.
     *
     * @Route("/{id}/edit")
     * @Method("GET")
     * @Template()
     * @ParamConverter("cmsType", class="CmsBundle:CmsType")
     */
    public function editAction(CmsType $cmsType)
    {
        if (!$cmsType) {
            throw $this->createNotFoundException('Unable to find CmsType entity.');
        }

        $editForm = $this->createForm(new CmsTypeType(), $cmsType);

        return array(
            'entity'      => $cmsType,
            'edit_form'   => $editForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }

    /**
     * Edits an existing CmsType entity.
     *
     * @Route("/{id}")
     * @Method("POST")
     * @Template()
     */
    public function updateAction(CmsType $cmsType)
    {
        $request = $this->getRequest();
        $editForm = $this->createForm(new CmsTypeType(), $cmsType);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cmsType);
            $em->flush();

            return $this->redirect($this->generateUrl('rocket_cms_cmstype_edit', array('id' => $cmsType->getId())));
        }

        return array(
            'entity'      => $cmsType,
            'edit_form'   => $editForm->createView(),
            'cms_backend_template' => $this->container->getParameter('cms.backend_template'),
        );
    }
}
